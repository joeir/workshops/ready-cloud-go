---
title: Ready, Cloud, GO! [Stack 2020]
theme: blood
separator: <!----->
verticalSeparator: <!--v-->
---

<!--
################
# INTRODUCTION #
################
-->

# READY,
# CLOUD,
# GO!

<!--v-->

### Human

- Enough experience with engineroom-level software development to write code
- Social manna to interact and participate in discussions

<!--v-->

### Machine

- 8 GB or more of RAM minimally
- 2 or more CPUs minially
- Docker Engine installed
- Kubernetes-in-Docker (KIND) installed

<!--v-->

### Objectives

1. Provide an (hopefully unopinionated) overview of what cloud-native really is
2. Debunk myths about *digital transformation plans* relying on adopting *cloud-native*
3. Enable you to make informed decisions when it comes to adopting cloud-native
4. Enable you to have a go at what an idealistic cloud-native environment looks like

<!----->
<!--
###############
# THE PROBLEM #
###############
-->

## The Problem

<!--v-->

<div style="display:block;text-align:center;width:100%;">
<small>

| Year | Population<sup>1</sup> | Internet Users<sup>2,3</sup> |
| --- | --- | --- |
| 2019 | 7,794,798,739 | ~4,390,000,000 |
| 2016 | 7,464,022,049 | 3,424,971,237 |
| 2013 | 7,210,581,976 | 2,728,428,107 | 
| 2010 | 6,956,823,603 | 2,023,202,974 |
| 2007 | 6,705,946,610 | 1,373,226,988 |
| 2004 | 6,461,159,389 | 913,327,771 |
| 2001 | 6,222,626,606 | 502,292,245 |
| 1998 | 5,984,793,942 | 188,507,628 |
| 1995 | 5,744,212,979 | 44,866,595 |
| 1992 | 5,498,919,809 | 14,121,924 |

> <small>Data from <sup>1</sup>[Worldometer](https://www.worldometers.info/world-population/world-population-by-year/), <sup>2</sup>[Internet Live Stats](https://www.internetlivestats.com/internet-users/), <sup>3</sup>[We Are Social](https://wearesocial.com/blog/2019/01/digital-2019-global-internet-use-accelerates)</small>

</small>
</div>

<!--v-->

#### Towards a Definition

<!--v-->

> ...focuses on how to optimize system architectures for the unique capabilities of the cloud...

<!--v-->

> ...focuses on achieving resilience and scale though horizontal scaling, distributed processing, and automating the replacement of failed components...

<!--v-->

> ...empower organizations to build and run scalable applications in modern, dynamic environments...

<!--v-->

> ...enable loosely coupled systems that are resilient, manageable, and observable...

<!--v-->

> ...allow engineers to make high-impact changes frequently and predictably with minimal toil...

<!--v-->

> ...is an approach to building and running applications that exploits the advantages of the cloud computing delivery model...

<!--v-->

> ...how applications are created and deployed, not where...

<!--v-->

...automatable containers at scale ... services optimized [sic] for change ... ubiquitous domain model ... well-crafted code...

<!--v-->

### Quantity | Scale

<!--v-->

### Diversity | Agility

<!--v-->

### Complexity | Resilience

<!--v-->

#### Lean by Eric Ries

<!--v-->

#### 3X by Kent Beck

- Explore
- Expand
- Extract

<!----->


<!--
###############
# METHODOLOGY #
###############
-->

## METHODOLOGY

<!--v-->

### Development

<!--v-->

#### Iterative Development

<small>

- Agile Scrum
- Extreme Programming

</small>

<!--v-->

#### 12-Factor Application

<small>

- One codebase tracked in revision control, many deploys
- Explicitly declare and isolate dependencies
- Store config in the environment
- Treat backing services as attached resources
- Strictly separate build and run stages
- Execute the app as one or more stateless processes
- Export services via port binding
- Scale out via the process model
- Maximize robustness with fast startup and graceful shutdown
- Keep development, staging, and production as similar as possible
- Treat logs as event streams
- Run admin/management tasks as one-off processes

</small>

<!--v-->

#### Trunk-Based Development

<small>

- Short-lived feature branches
- Merge into `master` on passing of tests

</small>

<!--v-->

### Arhictecture & Design

<!--v-->

#### Domain-Driven Design

<!--v-->

#### Service-Oriented Architecture

<!--v-->

### Quality

<!--v-->

#### Pre-Production Testing

- Service Virtualisation
- Contract-Based Testing

<!--v-->

#### Production Verification

- System Observability
- Monitoring & Alerting
- Chaos Engineering

<!--v-->

### Production

<!--v-->

#### Code Logistics

- Continuous Integration
- Continuous Delivery (CI/CD)
- Container-Based Deployments

<!--v-->

#### Operations

- Horizontal Scaling
- Zero-Trust Security Model

<!--v-->

#### Infrastructure

- Infrastructure-as-Code
- GitOps

<!----->
<!--
###########
# CULTURE #
###########
-->

## CULTURE

<!--v-->

#### Systems Thinking

<!--v-->

#### Feedback Cycles

<!--v-->

#### Continuous Learning & Experimentation

<!--v-->

#### User-Centricity

<!--v-->

#### Psychological Safety

<!----->
<!--
################
# MEASUREMENTS #
################
-->

## MEASUREMENTS

<!--v-->

#### Lead Time For Changes

<div style="display:block;text-align:left;width:100%;">
<small>

For the primary application or service you work on, what is your lead time for changes (i.e., how long does it take to go from code committed to code successfully running in production)?

</small>
</div>

<!--v-->

#### Deployment Frequency

<div style="display:block;text-align:left;width:100%;">
<small>

For the primary application or service you work on, how often does your organization deploy code to production or release it to end users?

</small>
</div>

<!--v-->

#### Mean Time To Recovery

<div style="display:block;text-align:left;width:100%;">
<small>

For the primary application or service you work on, how long does it generally take to restore service when a service incident or a defect that impacts users occurs (e.g., unplanned outage or service impairment)?

</small>
</div>

<!--v-->

#### Change Failure Rate

<div style="display:block;text-align:left;width:100%;">
<small>

For the primary application or service you work on, what percentage of changes to production or released to users result in degraded service (e.g., lead to service impairment or service outage) and subsequently require remediation (e.g., require a hotfix, rollback, fix forward, patch)?

</small>
</div>

<!--v-->

#### Psychological Safety

<div style="display:block;text-align:left;width:100%;">
<small>

- [ ] If you make a mistake on this team, it is often held against you
- [ ] Members of this team are able to bring up problems and tough issues
- [ ] People on this team sometimes reject others for being different
- [ ] It is safe to take a risk on this team
- [ ] It is difficult to ask other members of this team for help
- [ ] No one on this team would deliberately act in a way that undermines my efforts
- [ ] Working with members of this team, my unique skills and talents are valued and utilised

</small>
</div>

<!----->
<!--
#############
# RESOURCES #
#############
-->

## RESOURCES


<!----->

# THE
# END