# Ready, Cloud, GO!

> This repository contains courseware for the **Ready, Cloud, GO!** workshop at [Stack 2020 Developers Conference](https://www.govtechstack.sg/) scheduled to be held at Suntec Singapore Convention & Exhibition Center on 30 March 2020 10:00 - 13:00.



## \*That\* Sales Pitch

> The following is an excerpt from the workshop page on the official website which can be found at [https://www.govtechstack.sg/session/ready-cloud-go-cloud-ready-cloud-native](https://www.govtechstack.sg/session/ready-cloud-go-cloud-ready-cloud-native).

“Going to the cloud” is often spoken as something simple and inevitable. All we have to do is to save our existing services with a cloud provider and everything will be fine. Isn’t it? In this hands-on workshop, attendees will learn the various considerations that come into play, such as creating cloud-native services from scratch or migrating existing applications from traditional data centres to cloud native platforms. These changes involve all businesses, technical and cultural aspects of product delivery in the 21st century. So come prepared for a healthy dose of both conversation and code about what cloud-native is about.



## The Container

To be effective in delivery, workshop content needs to be written within certain boundaries. Here it is.

This particular workshop intends to:

1. Provide an (hopefully unopinionated) overview of what cloud-native really is
2. Debunk common myths about *that digital transformation plans* which relies on adopting *"cloud-native"*
3. Enable attendees to make informed decisions when it comes to adopting cloud-native
4. Equip attendees with an experience of what an idealistic cloud-native environment looks like

There will be **seven** overarching topics which we will explore, and these are:

1. Defining the problem / *what exactly are we solving for?*
2. Guiding principles / *what are the objectives & underlying intention?*
3. Constructive cultures / *what mindset is constructive to adopt?*
4. Useful methodologies / *what mental models can we adopt?*
5. Architecture and design / *how would a feasible plan look like?*
6. Tools / *what has been done and what can we leverage on?*
7. Measuring our state / *so, where are we now?*

As with all 3-hour workshops, time will not be on our side so pardon the over-idealistic examples that will be used (if you feel they are so). Additional reading materials and relevant training courses/certifications will also be covered at the end of this workshop, so chill and enjoy the journey!



## Pre-Requisites

### Human

- Enough experience with engineroom-level software development to write code
- Social manna to interact and participate in discussions

### Machine

- 8 GB or more of RAM minimally
- 2 or more CPUs minially
- Docker Engine installed
- Kubernetes-in-Docker (KIND) installed



## Table of Contents

- [Ready, Cloud, GO!](#ready-cloud-go)
  - [\*That\* Sales Pitch](#that-sales-pitch)
  - [The Container](#the-container)
  - [Pre-Requisites](#pre-requisites)
    - [Human](#human)
    - [Machine](#machine)
  - [Table of Contents](#table-of-contents)
- [Problem](#problem)
  - [Defining the Problem](#defining-the-problem)
  - [Issue #1: Scale](#issue-1-scale)
  - [Issue #2: Diversity](#issue-2-diversity)
  - [Issue #3: Complexity](#issue-3-complexity)
- [First Principles](#first-principles)
  - [Maximise service availability](#maximise-service-availability)
  - [Maximise business agility](#maximise-business-agility)
  - [Minimise risks from complexity](#minimise-risks-from-complexity)
- [Methodology](#methodology)
  - [Iterative Development](#iterative-development)
  - [Domain Driven Design](#domain-driven-design)
  - [Service-Oriented Architecture](#service-oriented-architecture)
  - [Automated Testing](#automated-testing)
  - [Continuous Integration](#continuous-integration)
  - [Continuous Delivery](#continuous-delivery)
  - [Container-Based Deployments](#container-based-deployments)
  - [Zero-Trust Security Model](#zero-trust-security-model)
  - [Container Orchestration](#container-orchestration)
  - [Infrastructure-as-Code](#infrastructure-as-code)
- [Culture](#culture)
  - [Psychological Safety](#psychological-safety)
  - [User-Centricity](#user-centricity)
  - [Systems Thinking](#systems-thinking)
  - [Amplified Feedback Cycles](#amplified-feedback-cycles)
  - [Continuous Experimentation & Learning](#continuous-experimentation--learning)
- [Tools](#tools)
  - [Kubernetes](#kubernetes)
  - [Docker](#docker)
  - [ArgoCD](#argocd)
  - [Gitlab](#gitlab)
- [Measurement](#measurement)
  - [Measurement: Pre-Questions](#measurement-pre-questions)
  - [Putting Numbers To Desirable Outcomes of Cloud-Native](#putting-numbers-to-desirable-outcomes-of-cloud-native)
    - [Lead Time To Change](#lead-time-to-change)
    - [Deployment Frequency](#deployment-frequency)
    - [Mean Time To Recovery](#mean-time-to-recovery)
    - [Change Failure Rate](#change-failure-rate)
  - [Measurement: Post-Questions](#measurement-post-questions)
- [More Resources](#more-resources)
  - [Web Links](#web-links)
  - [Books](#books)
  - [Training & Certifications](#training--certifications)
- [Acknowledgements](#acknowledgements)
- [Who the hell am I anyway?](#who-the-hell-am-i-anyway)
- [License](#license)



- - -



# Problem

> Pre-Section Reflection
> 
> - What is your current definition of 'cloud-native'?
> - What struggle brings you here today?
> - What problems do you believe adopting 'cloud-native' (whatever your current interpretation) will solve?



## Defining the Problem

**"If it ain't broke, don't fix it"**

We begin by questioning ourselves what problem *cloud-native* is solving, because if it's not solving any real problem, why do it?

So here's some data to get us started. What follows is a chart with data on the world population and the internet population by year in three-year intervals, and **if there was one chart that you took away from this workshop, this should be it:**

| Year | Population<sup>1</sup> | Internet Users<sup>2,3</sup> |
| --- | --- | --- |
| 2019 | 7,794,798,739 | ~4,390,000,000 |
| 2016 | 7,464,022,049 | 3,424,971,237 |
| 2013 | 7,210,581,976 | 2,728,428,107 | 
| 2010 | 6,956,823,603 | 2,023,202,974 |
| 2007 | 6,705,946,610 | 1,373,226,988 |
| 2004 | 6,461,159,389 | 913,327,771 |
| 2001 | 6,222,626,606 | 502,292,245 |
| 1998 | 5,984,793,942 | 188,507,628 |
| 1995 | 5,744,212,979 | 44,866,595 |
| 1992 | 5,498,919,809 | 14,121,924 |

> Data from <sup>1</sup>[Worldometer](https://www.worldometers.info/world-population/world-population-by-year/), <sup>2</sup>[Internet Live Stats](https://www.internetlivestats.com/internet-users/), <sup>3</sup>[We Are Social](https://wearesocial.com/blog/2019/01/digital-2019-global-internet-use-accelerates)

As is clearly shown, as of 2019, we have more than half of this planet who are connected to the internet whether through a desktop machine or a smart phone, with almost a third joining the party within the last 3 years. A hint of things to come: it's got to do with people.

At this point, let's also see what has been written about cloud-native by those who've arguably got their skin in the game for awhile now:

| Company | Quoted regarding cloud-native... |
| --- | --- |
| Google | ...focuses on how to optimize system architectures for the unique capabilities of the cloud...<sup>1</sup> |
| | ...focuses on achieving resilience and scale though horizontal scaling, distributed processing, and automating the replacement of failed components...<sup>1</sup> |
| CNCF | ...empower organizations to build and run scalable applications in modern, dynamic environments...<sup>2</sup> |
| | ...enable loosely coupled systems that are resilient, manageable, and observable...<sup>2</sup> |
| | ...allow engineers to make high-impact changes frequently and predictably with minimal toil...<sup>2</sup> |
| Pivotal | ...is an approach to building and running applications that exploits the advantages of the cloud computing delivery model...<sup>3</sup> |
| | ...how applications are created and deployed, not where...<sup>3</sup> |
| O'Reilly/Red Hat | ...automatable containers at scale...<sup>4</sup> |
| | ...services optimized [sic] for change...<sup>4</sup> |
| | ...ubiquitous domain model...<sup>4</sup> |
| | ...well-crafted code...<sup>4</sup> |

Sources: <sup>1</sup>[5 principles for cloud-native architecture—what it is and how to master it](https://cloud.google.com/blog/products/application-development/5-principles-for-cloud-native-architecture-what-it-is-and-how-to-master-it) <sup>2</sup>[Cloud-Native Computing Foundation Charter as of 2020](https://github.com/cncf/foundation/blob/master/charter.md) <sup>3</sup>[Cloud-Native Applications: Ship Faster, Reduce Risk, and Grow Your Business by Pivotal](https://pivotal.io/cloud-native) <sup>4</sup>[Kubernetes Patterns by Bilgan Ibryan & Roland Hub published by O’Reilly (2019)](https://www.redhat.com/en/resources/oreilly-kubernetes-patterns-cloud-native-apps)

From the above, we can see that cloud-native spans multiple areas of product development and is centered on *'exploiting cloud capabilities'*, indicating that simply being *on the cloud* does not equate to being *cloud-native*.

Which begs us to ask: What is the cloud? I'm sure you've heard of the joke, "There is no cloud, it's just someone elses computer," before - which is true. So why would you want to put your product on somebody else's computer instead of your own?

## Issue #1: Scale

> **Increasing number of users requires increasing system flexibility in order to respond to loads gracefully without breaking the bank.**

Cloud-native is first-and-foremost a response to the need to scale.

In the not-so-distant past, web services ran on bare-metal servers AKA computers connected to an internet-accessible network. Scaling up your computing capacity meant issuing a purchase order to a hardware shop including getting the necessary approvals, transporting these bulky machines to the data center, installing required dependencies on these systems, loading the latest version of our software onto them, and finally configuring the network to reach these new servers. At best with a simple setup, this could take days. Meanwhile, e-commerce was on a roll, and business needs such as seasonal sales/flash deals required that these needed to happen in minutes.

Scaling up the number of servers we have will result in increased costs to keep those servers running; And what benefits do these additional costs and load capacities bring when the need for that scale went back down? What can we do when we have loads of servers prepared for heavy loads, but we have no load? We sell that computing power of course! Meet 2002, Amazon, and their [Cloud Computing Infrastructure Model](https://aws.amazon.com/what-is-cloud-computing/), which eventually gave rise to AWS as we know it in 2006.

Cloud-native is both a result of, and a response to scale. Without the need to serve an increasing number of online, customers, servers would not have been bought and servers would not have been able to be shared during times of redundancy. Without being able to leverage on these economies-of-scale, we wouldn't have what we know today as "the cloud".

## Issue #2: Diversity

**Increasing customer-diversity requires an increased level of business agility in order to gain and retain market share.**

It's easy to design a product/service for a single target demographic. Reaching concensus amongst three people is a very different game from reaching the same with thousands. With great scale comes great diversity, and increasing diversity within a product's target users means that businesses will find it increasingly difficult to fully understand how their product/service brings value to their customers' way of life, giving rise to an age where user feedback-driven approaches have taken the spotlight (to investors at least) over visionary leaders who had uncanny intuitions for what users would love. [The Founder's Dilemma](https://hbr.org/2008/02/the-founders-dilemma) is real.

Technology-based businesses in their scaling phase have begun to realise that there is no real way to measure a feature's value until it's put in the hands of users. The faster we can get a feature to be used by real people, the faster we know whether that feature works or not. Having deployments limited to monthly visits to the data-center just wouldn't cut it.

Cloud-native addresses this need by enabling things such as frequent deployments which in turn enable businesses to learn about their customers both better and faster.

## Issue #3: Complexity

**Increasing product scale, complexity, and delivery demands results in technical debt, OR, new ways to keep services alive.**

When I decided on Computer Science as my way of life back in university, I was rather hoping that I could be left alone to my screens. How wrong I was; And my two main takeaways from the experience of being forced to have groupmates on a project who's scope was not decided by us, was:

1. **Likelihood of success is inversely correlated to number of team members**, and more importantly,
2. **No man is an island**, a product can only scale so far with one engineer

As technology businesses scale, the number of engineers on it would inevitably increase too. Conway's Law comes to mind. With scale comes complexity, and with complexity comes a time when nobody on the engineering team can know every part of the product.

- - -

# First Principles

> Pre-Section Reflection
> 
> - What is your current definition of 'cloud-native'?
> - What struggle brings you here today?
> - What problems do you believe adopting 'cloud-native' (whatever your current interpretation) will solve?

## Maximise service availability

A cloud-native service in theory should be resilient.

## Maximise business agility

Adopting a cloud-native approach to writing applications should enable a business to be agile in terms of business decision making. Features should be malleable as feedback is received and analysed by the organisation.

## Minimise risks from complexity

Adopting a cloud-native approach, if done correctly, should result in lowered risk of system failures due to complexity.


- - -

# Methodology

> **"So... How do we get it done?"**

## Iterative Development

## Domain Driven Design

## Service-Oriented Architecture

## Automated Testing

## Continuous Integration

## Continuous Delivery

## Container-Based Deployments

## Zero-Trust Security Model

## Container Orchestration

## Infrastructure-as-Code

- - -

# Culture

> **"Culture eats strategy for breakfast"**

## Psychological Safety

> "... is the shared belief that the work environment is safe for interpersonal risk taking"

Psychological safety as a concept was termed by Dr Amy C Edmonson in her book The Fearless Organisation

For each of the following points, rate how you currently feel on the Likert scale (1 - Strongly Disagree, 2 - Disagree, 3 - Neither Disagree/Agree, 4 - Agree, 5 - Strongly Agree):

- When someone makes a mistake in this team, it is often held against him or her.
- In this team, it is easy to discuss difficult issues and problems.
- In this team, people are sometimes rejected for being different.
- It is completely safe to take a risk on this team.
- It is difficult to ask other members of this team for help.
- Members of this team value and respect each others' contributions.

## User-Centricity

## Systems Thinking

## Amplified Feedback Cycles

## Continuous Experimentation & Learning

- - -

# Tools

## Kubernetes

## Docker

## ArgoCD

## Gitlab



- - -



# Measurement

> **"If you can't measure it, you can't improve it"**

## Measurement: Pre-Questions

- In an ideal world, what would you like to know about your product/service?
- What numbers are currently in use to quantify the quality of your product/service?
- Can you relate these numbers to your organisation/service's key value?

## Putting Numbers To Desirable Outcomes of Cloud-Native

In the book Accelerate, Forsgren and Kim defines four key metrics that I felt was useful when it came to quantifying desirable outcomes from adopting a cloud-native architecture. These are:

- Lead Time To Change
- Deployment Frequency
- Mean Time To Recovery
- Change Failure Rate

What I find beautiful about these numbers is that they measure the desired outcomes without being prescriptive in terms of exact methodologies IE do what works to reach those numbers!

### Lead Time To Change

> "... the time it takes to go from code committed to code successfully running in production ..."

Lead time to change measures how quickly changes can go from a development machine into production and serves as a healthy measure of business agility. "If you can dream it, you can build it" is meaningless if you can't get it to your users.

The goal here is to optimise your code logistics so that once a feature is pushed, it gets to be used by real users as quickly as possible, enabling other good things such as faster feedback from users.

### Deployment Frequency

> "... tells you how often you’re delivering something of value to end users and/or getting feedback from users ..."

Deployment frequency measures the interval between which new features can be pushed out to the user and is a 

### Mean Time To Recovery

### Change Failure Rate

## Measurement: Post-Questions

- What numbers are currently in use to quantify the quality of your people?



- - -



# More Resources

## Web Links

- [The Twelve-Factor App](https://12factor.net/)
- [Defining cloud native](https://docs.microsoft.com/en-us/dotnet/architecture/cloud-native/definition)
- [What are cloud-native applications?](https://pivotal.io/cloud-native)
- [5 principles for cloud-native architecture—what it is and how to master it](https://cloud.google.com/blog/products/application-development/5-principles-for-cloud-native-architecture-what-it-is-and-how-to-master-it)

## Books

- [Beyond the Twelve-Factor App: Exploring the DNA of Highly Scalable, Resilient Cloud Applications](https://www.goodreads.com/book/show/30460867-beyond-the-twelve-factor-app-exploring-the-dna-of-highly-scalable-resil)
- [Kubernetes Patterns: Reusable Elements for Designing Cloud-Native Applications](https://k8spatterns.io/)
- [Prove It!: How to Create a High-Performance Culture and Measurable Success](https://www.bookdepository.com/Prove-It-/9780730336228)
- [Accelerate: The Science of Lean Software and Devops: Building and Scaling High Performing Technology Organizations](https://www.bookdepository.com/Accelerate/9781942788331)
- [The Phoenix Project: A Novel About IT, DevOps, and Helping your Business Win](https://www.bookdepository.com/Phoenix-Project-Gene-Kim/9781942788294)
- [The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data](https://www.bookdepository.com/The-Unicorn-Project/9781942788768)
- [The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations](https://www.bookdepository.com/DevOPS-Handbook-Gene-Kim/9781942788003)

## Training & Certifications

- [Certified Kubernetes Administrator by CNCF/Linux Foundation](https://www.cncf.io/certification/cka/)
- [Certified Kubernetes Application Developer by CNCF/Linux Foundation](https://www.cncf.io/certification/ckad/)
- [Professional Cloud Architect by Google](https://cloud.google.com/certification/cloud-architect)
- [Professional Cloud Developer by Google](https://cloud.google.com/certification/cloud-developer)
- [AWS Certified Solutions Architect by Amazon Web Services](https://aws.amazon.com/certification/certified-solutions-architect-associate)

# Acknowledgements

Shout-out to the following people (check out their work!):

- Ryan Goh ([@ryanoolala](https://github.com/ryanoolala))
- Andrey Bodoev ([@shonoru](https://github.com/shonoru))
- Wong Yan Yee ([@houdinisparks](https://github.com/houdinisparks))
- Eric CE Tan ([LinkedIn](https://sg.linkedin.com/in/er1csg))
- Alvin Siew ([@alvinsiew](https://github.com/alvinsiew))
- Janice Tan ([@janicetyp](https://github.com/janicetyp))


# Who the hell am I anyway?

Good to see you at the bottom of this document. I'm Joseph Matthias Goh (don't google this) AKA @zephinzer (google this for my skunkwork) AKA @joeir (google this for polished stuff), a software engineer that's commonly mistakenly labelled by enterprise types as a DevOps engineer. I assist my team in the areas of code logistics and the main problem I tend to solve professionally is getting code from development to production as quickly and as safely as possible. When I'm not in front of a terminal, I do coaching/mentoring for software engineers and busy myself with public speaking experiences.

Outside of my professional work, I enjoy writing useful software without the constraints of linters and/or approvals, and I often amuse myself with developments in the software architecture and code modifiability domain. I'm also involved in the culture & methods space and am interested in kickstarting/scaling engineering culture that emphasises sustained engineering excellence.

If you'd like to engage me with small-talk, I also know basic Spanish, a little Chinese martial arts (Wing Chun), and dabble in music creation/recording/production in my free-time.

# License

Original content in this workshop is licensed under the [Creative Commons Attribution-ShareAlike 3.0 Singapore (CC BY-SA 3.0 SG)](https://creativecommons.org/licenses/by-sa/3.0/sg/). Efforts have been made to verify the source of other adapted content, and they are licensed under their original creators' licensing.
