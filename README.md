# Ready, Cloud, GO!

> This repository contains courseware for the **Ready, Cloud, GO!** workshop at [Stack 2020 Developers Conference](https://www.govtechstack.sg/) scheduled to be held at Suntec Singapore Convention & Exhibition Center on 30 March 2020 10:00 - 13:00.

- [Ready, Cloud, GO!](#ready-cloud-go)
  - [*That* Sales Pitch](#that-sales-pitch)
  - [The Container](#the-container)
  - [Pre-Requisites](#pre-requisites)
    - [Human](#human)
    - [Machine](#machine)
- [Defining the Problem](#defining-the-problem)
  - [Overview: Defining the Problem](#overview-defining-the-problem)
  - [Who's Bitin'?](#whos-bitin)
  - [Thinking About Product Development In 2020](#thinking-about-product-development-in-2020)
    - [3X: Explore, Expand, Extract](#3x-explore-expand-extract)
    - [Wardley Maps: Genesis, Custom, Product, Commodity](#wardley-maps-genesis-custom-product-commodity)
  - [Towards A Defintion](#towards-a-defintion)
  - [The Problem &amp; Desired Outcomes](#the-problem-amp-desired-outcomes)
    - [Quantity / Availability](#quantity--availability)
    - [Diversity / Agility](#diversity--agility)
    - [Complexity / Resiliency](#complexity--resiliency)
  - [In A Nutshell...](#in-a-nutshell)
- [Methodology](#methodology)
  - [Overview: Methodology](#overview-methodology)
  - [Development](#development)
    - [Iterative Development](#iterative-development)
    - [12-Factor Application](#12-factor-application)
    - [Trunk-Based Development](#trunk-based-development)
  - [Architecture &amp; Design](#architecture-amp-design)
    - [Domain-Driven Design](#domain-driven-design)
    - [Services-Oriented Architecture](#services-oriented-architecture)
  - [Quality](#quality)
    - [Testing in Development](#testing-in-development)
    - [Testing in Production](#testing-in-production)
  - [Production](#production)
    - [Code Logistics](#code-logistics)
    - [Operations](#operations)
    - [Infrastructure](#infrastructure)
- [Culture](#culture)
  - [Overview: Culture](#overview-culture)
  - [Systems Thinking](#systems-thinking)
  - [Amplification of Feedback Cycles](#amplification-of-feedback-cycles)
  - [Continuous Experimentation &amp; Learning](#continuous-experimentation-amp-learning)
  - [User-Centricity](#user-centricity)
  - [Psychological Safety](#psychological-safety)
- [Measurements](#measurements)
  - [Overview: Measurements](#overview-measurements)
  - [Technical Metrics](#technical-metrics)
    - [Lead Time For Changes](#lead-time-for-changes)
    - [Deployment Frequency](#deployment-frequency)
    - [Mean Time To Recovery](#mean-time-to-recovery)
    - [Change Failure Rate](#change-failure-rate)
  - [People Metrics](#people-metrics)
    - [Psychological Safety](#psychological-safety-1)
    - [Personality Fits](#personality-fits)
      - [Big Five Personality](#big-five-personality)
      - [StrengthsFinder](#strengthsfinder)
- [Resources](#resources)
  - [Videos](#videos)
  - [Web Links](#web-links)
  - [Books](#books)
  - [Training &amp; Certifications](#training-amp-certifications)
- [Acknowledgements](#acknowledgements)
- [Who the hell am I anyway?](#who-the-hell-am-i-anyway)
- [License](#license)



## \*That\* Sales Pitch

> The following is an excerpt from the workshop page on the official website which can be found at [https://www.govtechstack.sg/session/ready-cloud-go-cloud-ready-cloud-native](https://www.govtechstack.sg/session/ready-cloud-go-cloud-ready-cloud-native).

“Going to the cloud” is often spoken as something simple and inevitable. All we have to do is to save our existing services with a cloud provider and everything will be fine. Isn’t it? In this hands-on workshop, attendees will learn the various considerations that come into play, such as creating cloud-native services from scratch or migrating existing applications from traditional data centres to cloud native platforms. These changes involve all businesses, technical and cultural aspects of product delivery in the 21st century. So come prepared for a healthy dose of both conversation and code about what cloud-native is about.



## The Container

To be effective in delivery, workshop content needs to be written within certain boundaries. Here it is.

This particular workshop intends to:

1. Provide an (hopefully unopinionated) overview of what cloud-native really is
2. Debunk common myths about *that digital transformation plans* which relies on adopting *"cloud-native"*
3. Enable attendees to make informed decisions when it comes to adopting cloud-native
4. Equip attendees with an experience of what an idealistic cloud-native environment looks like

There will be **four** overarching topics which we will explore, and these are:

1. Defining the problem / *what exactly are we solving for?*
2. Useful methodologies / *what mental models can we adopt?*
3. Constructive cultures / *what mindset is constructive to adopt?*
4. Measuring our state / *so, where are we now?*

As with all 3-hour workshops, time will not be on our side so pardon the over-idealistic examples that will be used (if you feel they are so). Additional reading materials and relevant training courses/certifications will also be covered at the end of this workshop, so chill and enjoy the journey!



## Pre-Requisites

### Human

- Enough experience with engineroom-level software development to write code
- Social manna to interact and participate in discussions

### Machine

- 8 GB or more of RAM minimally
- 2 or more CPUs minially
- Docker Engine installed
- Kubernetes-in-Docker (KIND) installed


- - -


# Defining the Problem

> Before we begin this section, try noting down for yourself:
> 
> - What struggle brings you here?
> - How would you define *cloud-native* for yourself?
> - How could/would *cloud-native* address your struggles?

## Overview: Defining the Problem

When approaching any new concept, I've found it's usually constructive to ask myself: **what problem is really being solved here**?

It's too easy to get excited about something new and shiny, forgetting in the first place why we do what we currently do. In this section on Defining the Problem, I will be sharing on how I think application development is evolving, and what I think is the crux of the situation that calls for a new way of creating software.

## Who's Bitin'?

If there's got to be a single set of data that I feel highlights the problem best, it would be the following table.

| Year | Population<sup>1</sup> | Internet Users<sup>2,3</sup> |
| --- | --- | --- |
| 2019 | 7,794,798,739 | ~4,390,000,000 |
| 2016 | 7,464,022,049 | 3,424,971,237 |
| 2013 | 7,210,581,976 | 2,728,428,107 | 
| 2010 | 6,956,823,603 | 2,023,202,974 |
| 2007 | 6,705,946,610 | 1,373,226,988 |
| 2004 | 6,461,159,389 | 913,327,771 |
| 2001 | 6,222,626,606 | 502,292,245 |
| 1998 | 5,984,793,942 | 188,507,628 |
| 1995 | 5,744,212,979 | 44,866,595 |
| 1992 | 5,498,919,809 | 14,121,924 |

> Data from <sup>1</sup>[Worldometer](https://www.worldometers.info/world-population/world-population-by-year/), <sup>2</sup>[Internet Live Stats](https://www.internetlivestats.com/internet-users/), <sup>3</sup>[We Are Social](https://wearesocial.com/blog/2019/01/digital-2019-global-internet-use-accelerates)



## Thinking About Product Development In 2020

Here are two

### 3X: Explore, Expand, Extract

![Source: https://sketchingscrummaster.com/2018/06/11/kent-beck-the-product-development-triathlon/](./.assets/3x-overview.png)

### Wardley Maps: Genesis, Custom, Product, Commodity

![Source: https://productanonymous.com/2019/03/the-what-why-of-wardley-maps-summary/](.assets/wardley-map.jpg)

## Towards A Defintion

| Company | Quoted regarding cloud-native... |
| --- | --- |
| Google | ...focuses on how to optimize system architectures for the unique capabilities of the cloud...<sup>1</sup> |
| | ...focuses on achieving resilience and scale though horizontal scaling, distributed processing, and automating the replacement of failed components...<sup>1</sup> |
| CNCF | ...empower organizations to build and run scalable applications in modern, dynamic environments...<sup>2</sup> |
| | ...enable loosely coupled systems that are resilient, manageable, and observable...<sup>2</sup> |
| | ...allow engineers to make high-impact changes frequently and predictably with minimal toil...<sup>2</sup> |
| Pivotal | ...is an approach to building and running applications that exploits the advantages of the cloud computing delivery model...<sup>3</sup> |
| | ...how applications are created and deployed, not where...<sup>3</sup> |
| O'Reilly/Red Hat | ...automatable containers at scale...<sup>4</sup> |
| | ...services optimized [sic] for change...<sup>4</sup> |
| | ...ubiquitous domain model...<sup>4</sup> |
| | ...well-crafted code...<sup>4</sup> |

> Sources: <sup>1</sup>[5 principles for cloud-native architecture—what it is and how to master it](https://cloud.google.com/blog/products/application-development/5-principles-for-cloud-native-architecture-what-it-is-and-how-to-master-it) <sup>2</sup>[Cloud-Native Computing Foundation Charter as of 2020](https://github.com/cncf/foundation/blob/master/charter.md) <sup>3</sup>[Cloud-Native Applications: Ship Faster, Reduce Risk, and Grow Your Business by Pivotal](https://pivotal.io/cloud-native) <sup>4</sup>[Kubernetes Patterns by Bilgan Ibryan & Roland Hub published by O’Reilly (2019)](https://www.redhat.com/en/resources/oreilly-kubernetes-patterns-cloud-native-apps)

## The Problem & Desired Outcomes

### Quantity / Availability

As we've seen, cloud-native is primarily a response to scale. 

### Diversity / Agility

Another problem that large groups of people bring is the relative difficulty to reach concensus. 

### Complexity / Resiliency

## In A Nutshell...

To summarise this section on **Defining the Problem**, cloud-native is essentially a response to the need to scale. More users causes usage patterns to be more accentuated, increasing the need for ease of scalability. Along with these users also comes diversity, demanding that our systems stay nimble enough to enable the business to respond to customer needs. Finally, with a scaling product comes complexity and the need for structures and practices to manage the risks that come with this necessary complexity.

> - What phase of development is your product/service at?
> - Which of the problems are most relevant to your product/service?
> - Which of the desired outcomes would benefit your product/service the most?


- - -


# Methodology

> Before we begin this section, try noting down for yourself:
> 
> - How would you describe a *cloud-native* system/application?
> - What are some concepts/models you consider *cloud-native*?
> - What frameworks do you use when thinking *cloud-native*?

## Overview: Methodology

## Development

### Iterative Development
- Agile Scrum
- Extreme Programming

### 12-Factor Application

- One codebase tracked in revision control, many deploys
- Explicitly declare and isolate dependencies
- Store config in the environment
- Treat backing services as attached resources
- Strictly separate build and run stages
- Execute the app as one or more stateless processes
- Export services via port binding
- Scale out via the process model
- Maximize robustness with fast startup and graceful shutdown
- Keep development, staging, and production as similar as possible
- Treat logs as event streams
- Run admin/management tasks as one-off processes

### Trunk-Based Development

- Short-lived feature branches
- Merge into `master` on passing of tests

## Architecture & Design

### Domain-Driven Design

### Services-Oriented Architecture

- Microservices
- Serverless

## Quality

### Testing in Development

- Contract-Based Testing
- Load testing/Benchmarking
- Service Virtualisation

### Testing in Production

- System Observability
- Monitoring & Alerting
- Chaos Engineering

## Production

### Code Logistics

- Continuous Integration/Delivery (CI/CD)
- Container-Based Deployments

### Operations

- Horizontal Scaling
- Zero-Trust Security Model

### Infrastructure

- Infrastructure-as-Code
- GitOps

> - Which of these methodologies are already in play at your organisation?
> - Which of these methodologies would benefit your product/service the most?
> - Which of these methodologies seem unfeasible for your context and why?


- - -


# Culture

## Overview: Culture

> "Culture eats strategy for breakfast" - Peter Drucker

We can implement all the processes we want, and ultimately, technology and software will always be created for people, by people. Culture I would argue, is far more important than methodolgy when it comes to delivering value in a cloud-native environment.

The following section is based on the works of Gene Kim (Phonenix Project/DevOps Handbook) and Amy C Edmondson (The Fearless Organization), and is what I've found useful when assessing my own team and others in my day-to-day work. Those who've read the books would

## Systems Thinking

Systems thinking advocates placing emphasis on the entire flow of work from conceptualization to production. Essentially, having a culture where systems thinking is promoted enables us to discover global maximas while optimising our processes instead of considering just the areas we are concerned with.

## Amplification of Feedback Cycles

Amplification of feedback cycles concerns itself with how quickly each component in a process realises the outcome of it's being.

## Continuous Experimentation & Learning

Also known as the third DevOps way as seen in The Phoenix Project,

## User-Centricity

## Psychological Safety

"... a shared belief held by members of a team that the team is safe for interpersonal risk taking.”

- - -


# Measurements

## Overview: Measurements

## Technical Metrics

### Lead Time For Changes

Lead time for changes is defined as: **For the primary application or service you work on, what is your lead time for changes (i.e., how long does it take to go from code committed to code successfully running in production)?**

"Elite" organisations (as classified in the 2019 Accelerate State of DevOps Report) achieves a lead time of **less than a day**.

### Deployment Frequency

Deployment frequency is defined as: **For the primary application or service you work on, how often does your organization deploy code to production or release it to end users?**

"Elite" organisations (as classified in the 2019 Accelerate State of DevOps Report) achieves a frequency of **multiple times a day (on-demand)**.

### Mean Time To Recovery

Mean time to recovery is defined as: **For the primary application or service you work on, how long does it generally take to restore service when a service incident or a defect that impacts users occurs (e.g., unplanned outage or service impairment)?**

"Elite" organisations (as classified in the 2019 Accelerate State of DevOps Report) achieves a lead time of **less than an hour**.

### Change Failure Rate

Change failure rate is defined as: **For the primary application or service you work on, what percentage of changes to production or released to users result in degraded service (e.g., lead to service impairment or service outage) and subsequently require remediation (e.g., require a hotfix, rollback, fix forward, patch)?**

"Elite" organisations (as classified in the 2019 Accelerate State of DevOps Report) achieves a failure rate of **0-15%**.

## People Metrics

### Psychological Safety

⁃ If you make a mistake on this team, it is often held against you
⁃ Members of this team are able to bring up problems and tough issues
⁃ People on this team sometimes reject others for being different
⁃ It is safe to take a risk on this team
⁃ It is difficult to ask other members of this team for help
⁃ No one on this team would deliberately act in a way that undermines my efforts
⁃ Working with members of this team, my unique skills and talents are valued and utilised

### Personality Fits

#### Big Five Personality

#### StrengthsFinder

- Informative for coaching purposes



- - -


# Resources



## Videos

- [YOW! Conference 2018 - Kent Beck - 3x Explore, Expand, Extract](https://www.youtube.com/watch?v=FlJN6_4yI2A)



## Web Links

- [CNCF Landscape](https://landscape.cncf.io/)
- [Defining cloud native](https://docs.microsoft.com/en-us/dotnet/architecture/cloud-native/definition)
- [What are cloud-native applications?](https://pivotal.io/cloud-native)
- [5 principles for cloud-native architecture—what it is and how to master it](https://cloud.google.com/blog/products/application-development/5-principles-for-cloud-native-architecture-what-it-is-and-how-to-master-it)
- [2019 Accelerate State of DevOps Report](https://cloud.google.com/devops/state-of-devops/)
- [The Twelve-Factor App](https://12factor.net/)
- [Trunk-Based Development](https://trunkbaseddevelopment.com/)
- [Wardley Maps (Simon Wardley's)](https://medium.com/wardleymaps)
- [The What & Why of Wardley Maps](https://productanonymous.com/2019/03/the-what-why-of-wardley-maps-summary/)
- [Google re:Work](https://rework.withgoogle.com/print/guides/5721312655835136/)
- [Big Five Personality](https://www.ocf.berkeley.edu/~johnlab/bfi.php)
- [CliftonStrengths](https://www.gallup.com/cliftonstrengths/en/strengthsfinder.aspx)


## Books

- [Accelerate: The Science of Lean Software and Devops: Building and Scaling High Performing Technology Organizations](https://www.bookdepository.com/Accelerate/9781942788331)
- [The Fearless Organization: Creating Psychological Safety in the Workplace for Learning, Innovation, and Growth](https://www.bookdepository.com/Fearless-Organization-Amy-C-Edmondson/9781119477242)
- [Prove It!: How to Create a High-Performance Culture and Measurable Success](https://www.bookdepository.com/Prove-It-/9780730336228)
- [The Phoenix Project: A Novel About IT, DevOps, and Helping your Business Win](https://www.bookdepository.com/Phoenix-Project-Gene-Kim/9781942788294)
- [The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data](https://www.bookdepository.com/The-Unicorn-Project/9781942788768)
- [The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations](https://www.bookdepository.com/DevOPS-Handbook-Gene-Kim/9781942788003)
- [Beyond the Twelve-Factor App: Exploring the DNA of Highly Scalable, Resilient Cloud Applications](https://www.goodreads.com/book/show/30460867-beyond-the-twelve-factor-app-exploring-the-dna-of-highly-scalable-resil)
- [Kubernetes Patterns: Reusable Elements for Designing Cloud-Native Applications](https://k8spatterns.io/)



## Training & Certifications

- [Certified Kubernetes Administrator by CNCF/Linux Foundation](https://www.cncf.io/certification/cka/)
- [Certified Kubernetes Application Developer by CNCF/Linux Foundation](https://www.cncf.io/certification/ckad/)
- [Professional Cloud Architect by Google](https://cloud.google.com/certification/cloud-architect)
- [Professional Cloud Developer by Google](https://cloud.google.com/certification/cloud-developer)
- [AWS Certified Solutions Architect by Amazon Web Services](https://aws.amazon.com/certification/certified-solutions-architect-associate)



- - -



# Acknowledgements

Shout-out to the following people (check out their work!):

- Ryan Goh ([@ryanoolala](https://github.com/ryanoolala))
- Andrey Bodoev ([@shonoru](https://github.com/shonoru))
- Wong Yan Yee ([@houdinisparks](https://github.com/houdinisparks))
- Eric CE Tan ([LinkedIn](https://sg.linkedin.com/in/er1csg))
- Alvin Siew ([@alvinsiew](https://github.com/alvinsiew))
- Janice Tan ([@janicetyp](https://github.com/janicetyp))


- - -


# Who the hell am I anyway?

Good to see you at the bottom of this document. I'm Joseph Matthias Goh (don't google this) AKA @zephinzer (google this for my skunkwork) AKA @joeir (google this for polished stuff), a software engineer that's commonly mistakenly labelled by enterprise types as a DevOps engineer. I assist my team in the areas of code logistics and the main problem I tend to solve professionally is getting code from development to production as quickly and as safely as possible. When I'm not in front of a terminal, I do coaching/mentoring for software engineers and busy myself with public speaking experiences.

Outside of my professional work, I enjoy writing useful software without the constraints of linters and/or approvals, and I often amuse myself with developments in the software architecture and code modifiability domain. I'm also involved in the culture & methods space and am interested in kickstarting/scaling engineering culture that emphasises sustained engineering excellence.

If you'd like to engage me with small-talk, I also know basic Spanish, a little Chinese martial arts (Wing Chun), and dabble in music creation/recording/production in my free-time.


- - -


# License

Original content in this workshop is licensed under the [Creative Commons Attribution-ShareAlike 3.0 Singapore (CC BY-SA 3.0 SG)](https://creativecommons.org/licenses/by-sa/3.0/sg/). Efforts have been made to verify the source of other adapted content, and they are licensed under their original creators' licensing.
